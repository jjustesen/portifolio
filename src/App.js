import React from 'react'

import GlobalStyle from './styles/globalStyles'
import { ParallaxBanner } from 'react-scroll-parallax'
import { NavigationBar } from './components/molecules/NavigationBar/NavigationBar'
import styled from 'styled-components'
import { Description } from './components/molecules/Description'
import { FluxFlex } from './components/elements/Flex/Flex'
import { FluxBox } from './components/elements/Box'
import Title from './components/molecules/Title/Title'
import { ImageGroup } from './components/molecules/ImageGroup'
import ImageOne from './Images/ImageOne.jpg'
import ImageTwo from './Images/ImageTwo.jpg'
import ImageThree from './Images/ImageThree.jpg'
import { motion, useTransform, useViewportScroll } from 'framer-motion'

const SceneOne = styled.div`
  max-width: 1920px;
  height: 81vh;
`
const SceneTwo = styled.div`
  max-width: 1920px;
  height: 81vh;
  .parallax-banner-layer-0 {
    transform: translate(0px, 370px);
  }
  .parallax-inner {
    height: 100vh;
  }
`
const SceneFull = styled(FluxFlex)`
  max-width: 1920px;
  height: 100vh;
`

const SceneTitle = styled(FluxFlex)`
  max-width: 600px;
  height: 80vh;
  justify-content: center;
  align-items: center;
  margin-left: auto;
  margin-right: auto;
`

const BarScale = styled(motion.div)`
  width: 300px;
  height: 4px;
  background-color: #c4c4c4;
  margin-top: 32px;
`

function App() {
  window.onbeforeunload = function () {
    window.scrollTo(0, 0)
  }

  const { scrollYProgress } = useViewportScroll()
  const yRange1 = useTransform(scrollYProgress, [0.2, 0.3], [0, 1])
  const yRange2 = useTransform(scrollYProgress, [0.4, 0.5], [0, 1])
  const yRange3 = useTransform(scrollYProgress, [0.8, 0.9], [0, 1])

  return (
    <>
      <GlobalStyle />
      <SceneOne>
        <NavigationBar></NavigationBar>
        <FluxBox position="absolute" top="30%" ml={192} width={700}>
          <Title>Lorem ipsum dolor sit amet,</Title>
        </FluxBox>
      </SceneOne>
      <SceneTwo>
        <ParallaxBanner
          layers={[{ amount: 0.4, image: ImageOne }]}
          style={{
            height: '100%'
          }}
        />
      </SceneTwo>
      <SceneFull mt={300} alignItems="center" justifyContent="center">
        <FluxFlex alignItems="center">
          <FluxBox borderRadius={18} width={456} height={700} bg="red" overflow="hidden">
            <ParallaxBanner
              layers={[{ amount: 0.4, image: ImageTwo }]}
              style={{
                height: '100%'
              }}
            />
          </FluxBox>
          <FluxBox ml={300}>
            <Description />
            <BarScale
              style={{
                scaleX: yRange1
              }}
            />
          </FluxBox>
        </FluxFlex>
      </SceneFull>
      <SceneTitle>
        <Title textAlign="center">volutpat luctus. Curabitur</Title>
      </SceneTitle>
      <SceneFull justifyContent="center">
        <FluxFlex alignItems="center">
          <FluxBox>
            <Description />
            <BarScale
              style={{
                scaleX: yRange2
              }}
            />
          </FluxBox>
          <FluxBox ml={300}>
            <ImageGroup />
          </FluxBox>
        </FluxFlex>
      </SceneFull>
      <SceneTitle>
        <Title textAlign="center">volutpat luctus. Curabitur</Title>
      </SceneTitle>
      <SceneFull mt={300} alignItems="center" justifyContent="center">
        <FluxFlex alignItems="center">
          <FluxBox borderRadius={18} width={456} height={700} bg="red" overflow="hidden">
            <ParallaxBanner
              layers={[{ amount: 0.4, image: ImageThree }]}
              style={{
                height: '100%'
              }}
            />
          </FluxBox>
          <FluxBox ml={300}>
            <Description />
            <BarScale
              style={{
                scaleX: yRange3
              }}
            />
          </FluxBox>
        </FluxFlex>
      </SceneFull>
      <SceneTitle>
        <Title textAlign="center">volutpat luctus. Curabitur</Title>
      </SceneTitle>
    </>
  )
}

export default App
