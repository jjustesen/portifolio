import React from 'react'
import styled from 'styled-components'
import { FluxFlex } from '../../elements/Flex/Flex'
import { FluxText } from '../../elements/Text/Text'

const DescriptionText = styled(FluxText)`
  font-family: Montserrat Alternates;
  font-style: normal;
  font-weight: 500;
  font-size: 36px;
  line-height: 105.4%;

  max-width: 550px;
`

export const Description = () => {
  return (
    <FluxFlex flexDirection="column">
      <DescriptionText>
        Aenean in mattis arcu, eu consequat nunc. Fusce tristique ante purus, a facilisis augue tristique a.
      </DescriptionText>
    </FluxFlex>
  )
}

export default Description

Description.displayName = 'Description'

Description.defaultProps = {}
