import styled from 'styled-components'
import { FluxText } from '../../elements/Text/Text'

export const Title = styled(FluxText)`
  font-family: Montserrat Alternates;
  font-style: normal;
  font-weight: bold;
  font-size: 72px;
  line-height: 105.4%;
`

export default Title

Title.displayName = 'Title'

Title.defaultProps = {}
