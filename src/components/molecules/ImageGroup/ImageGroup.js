import React from 'react'
import { ParallaxBanner } from 'react-scroll-parallax'
import { FluxBox } from '../../elements/Box/Box'
import { FluxFlex } from '../../elements/Flex/Flex'
import Image1 from '../../../Images/ImagesGroup/foto_1.jpg'
import Image2 from '../../../Images/ImagesGroup/foto_2.jpg'
import Image3 from '../../../Images/ImagesGroup/foto_3.jpg'
import Image4 from '../../../Images/ImagesGroup/foto_4.jpg'
import Image5 from '../../../Images/ImagesGroup/foto_5.jpg'
import Image6 from '../../../Images/ImagesGroup/foto_6.jpg'
import Image7 from '../../../Images/ImagesGroup/foto_7.jpg'
import Image8 from '../../../Images/ImagesGroup/foto_8.jpg'
import Image9 from '../../../Images/ImagesGroup/foto_9.jpg'

export const ImageGroup = () => {
  return (
    <FluxFlex>
      <FluxFlex flexDirection="column">
        <FluxBox overflow="hidden" mt={140} mb={45} bg="grey" width={185} height={283}>
          <ParallaxBanner
            layers={[{ amount: 0.4, image: Image1 }]}
            style={{
              height: '100%'
            }}
          />
        </FluxBox>
        <FluxBox mb={45} overflow="hidden" bg="grey" width={185} height={283}>
          <ParallaxBanner
            layers={[{ amount: 0.4, image: Image2 }]}
            style={{
              height: '100%'
            }}
          />
        </FluxBox>
        <FluxBox mb={45} overflow="hidden" bg="grey" width={185} height={283}>
          <ParallaxBanner
            layers={[{ amount: 0.4, image: Image3 }]}
            style={{
              height: '100%'
            }}
          />
        </FluxBox>
      </FluxFlex>
      <FluxFlex ml={41} mr={41} flexDirection="column">
        <FluxBox mb={45} overflow="hidden" bg="grey" width={185} height={283}>
          <ParallaxBanner
            layers={[{ amount: 0.4, image: Image4 }]}
            style={{
              height: '100%'
            }}
          />
        </FluxBox>
        <FluxBox mb={45} overflow="hidden" bg="grey" width={185} height={283}>
          <ParallaxBanner
            layers={[{ amount: 0.4, image: Image5 }]}
            style={{
              height: '100%'
            }}
          />
        </FluxBox>
        <FluxBox mb={45} overflow="hidden" bg="grey" width={185} height={283}>
          <ParallaxBanner
            layers={[{ amount: 0.4, image: Image6 }]}
            style={{
              height: '100%'
            }}
          />
        </FluxBox>
      </FluxFlex>
      <FluxFlex flexDirection="column">
        <FluxBox mt={140} overflow="hidden" mb={45} bg="grey" width={185} height={283}>
          <ParallaxBanner
            layers={[{ amount: 0.4, image: Image7 }]}
            style={{
              height: '100%'
            }}
          />
        </FluxBox>
        <FluxBox mb={45} overflow="hidden" bg="grey" width={185} height={283}>
          <ParallaxBanner
            layers={[{ amount: 0.4, image: Image8 }]}
            style={{
              height: '100%'
            }}
          />
        </FluxBox>
        <FluxBox mb={45} overflow="hidden" bg="grey" width={185} height={283}>
          <ParallaxBanner
            layers={[{ amount: 0.4, image: Image9 }]}
            style={{
              height: '100%'
            }}
          />
        </FluxBox>
      </FluxFlex>
    </FluxFlex>
  )
}

export default ImageGroup
ImageGroup.displayName = 'ImageGroup'

ImageGroup.defaultProps = {}
