import React from 'react'
import styled from 'styled-components'
import { FluxBox } from '../../elements/Box/Box'
import { FluxText } from '../../elements/Text/Text'

const BoxAbsolut = styled(FluxBox)`
  position: absolute;
  top: 30%;
`

const SubTitleText = styled(FluxText)`
  font-family: Montserrat Alternates;
  font-style: normal;
  font-weight: bold;
  font-size: 72px;
  line-height: 105.4%;
`

export const SubTitle = () => {
  return (
    <BoxAbsolut ml={192} width={700}>
      <SubTitleText>Lorem ipsum dolor sit amet,</SubTitleText>
    </BoxAbsolut>
  )
}

export default SubTitle

SubTitle.displayName = 'SubTitle'

SubTitle.defaultProps = {}
