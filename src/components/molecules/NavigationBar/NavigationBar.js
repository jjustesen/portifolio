import React from 'react'
import { FluxBox } from '../../elements/Box/Box'
import { FluxFlex } from '../../elements/Flex/Flex'
import { FluxText } from '../../elements/Text/Text'

export const NavigationBar = () => {
  return (
    <FluxBox height={100} width="100%">
      <FluxFlex justifyContent="space-between">
        <FluxText m={40}>portissot</FluxText>
        <FluxText m={40}>menu</FluxText>
      </FluxFlex>
    </FluxBox>
  )
}

export default NavigationBar
NavigationBar.displayName = 'NavigationBar'

NavigationBar.defaultProps = {}
