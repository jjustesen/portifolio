import styled from 'styled-components'
import { layout, border, shadow, position, space, color } from 'styled-system'
import systemPropTypes from '@styled-system/prop-types'

export const fluxBoxPropTypes = {
  ...systemPropTypes.layout,
  ...systemPropTypes.border,
  ...systemPropTypes.shadow,
  ...systemPropTypes.position,
  ...systemPropTypes.space,
  ...systemPropTypes.color
}

export const FluxBox = styled.div`
  ${layout};
  ${border};
  ${shadow};
  ${position};
  ${space};
  ${color};
`

FluxBox.displayName = 'FluxBox'

FluxBox.propTypes = fluxBoxPropTypes

FluxBox.defaultProps = {}
