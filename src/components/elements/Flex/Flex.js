import styled from 'styled-components'
import { flexbox, border } from 'styled-system'
import systemPropTypes from '@styled-system/prop-types'

import { FluxBox } from '../../elements/Box'

export const FluxFlex = styled(FluxBox)`
  ${flexbox};
  ${border};
`

FluxFlex.displayName = 'FluxFlex'

FluxFlex.propTypes = {
  ...FluxBox.propTypes,
  ...systemPropTypes.flexbox
}

FluxFlex.defaultProps = {
  display: 'flex'
}
