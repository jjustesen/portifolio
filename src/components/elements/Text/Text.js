import PropTypes from 'prop-types'

import styled from 'styled-components'
import { space, color, fontFamily, fontWeight, textAlign, letterSpacing, fontSize } from 'styled-system'
import systemPropTypes from '@styled-system/prop-types'

export const FluxText = styled.span`
  ${fontSize};
  ${space};
  ${color};
  ${fontFamily};
  ${fontWeight};
  ${textAlign};
  ${letterSpacing};
`

FluxText.displayName = 'FluxText'

FluxText.propTypes = {
  truncate: PropTypes.bool,
  ...systemPropTypes.space,
  ...systemPropTypes.color,
  ...systemPropTypes.typography
}

FluxText.defaultProps = {
  variant: 'default',
  fontFamily: 'default'
}
